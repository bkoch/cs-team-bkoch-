// Project Euler poker hands 54.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <ctime>
using namespace std;

vector<vector<int>> handPairFreqRank= { { 1,1,1,1,1 },{ 2,1,1,1 },{ 2,2,1 },{ 3,1,1 },{},{},{ 3,2 },{ 4,1 } };

class PokerHand
{
private:
	friend bool operator>(PokerHand &player1, PokerHand &player2);
	friend bool getSameScoreWinner(PokerHand &player1Hand, PokerHand &player2Hand, int playerScores);
	
	int handScore;
	string handCards;
	vector<int> cardFreqVector;
	vector<int> cardValueVector;
	bool allSuitsTheSame;

	int getCardValueFromChar(char ref);
	bool isValueInVector(int ref);
	int getValuePositionInVector(int ref);
	void sortPokerHandVectors();
	void addValueToVectors(char cardValue);
	void updateAllSuitsSame(char cardSuit, char firstCardSuit);

	void setHandScore();

	int getPairScore();
	int getStraightOrFlushScore();

	bool isStraight();
	bool isFlush();
	bool isStraightFlush();
	bool isRoyalFlush();

public:
	PokerHand(string handCards);
	int getHandScore();

};

PokerHand::PokerHand(string hand)
{
	handCards = hand;
	char firstCardSuit;

	for (int i = 0; i < handCards.size(); i += 2)
	{

		addValueToVectors(handCards[i]);

		if (i<2)
		{
			firstCardSuit = handCards[i + 1];
		}
		else
		{
			updateAllSuitsSame(handCards[i + 1], firstCardSuit);
		}
	}

	sortPokerHandVectors();

	setHandScore();
}


bool operator>(PokerHand &player1Hand, PokerHand &player2Hand)
{
	if (player1Hand.getHandScore() > player2Hand.getHandScore())
	{
		return 1;

	}
	else if (player1Hand.getHandScore() < player2Hand.getHandScore())
	{
		return 0;

	}
	else
	{
		bool rez = getSameScoreWinner(player1Hand, player2Hand, player1Hand.getHandScore());
		if (rez == 1)
		{
			return 1;

		}
		else
		{
			return 0;
		}
	}
}

bool getSameScoreWinner(PokerHand &player1Hand, PokerHand &player2Hand, int playerScores)
{
	for (int i = 0; i < player1Hand.cardValueVector.size(); ++i)
	{
		if (player1Hand.cardValueVector[i]>player2Hand.cardValueVector[i])
		{
			return 1;
		}
		else if (player1Hand.cardValueVector[i]<player2Hand.cardValueVector[i])
		{
			return 0;
		}
	}

	throw exception("exactly the same value hands");
}

void PokerHand::setHandScore()
{
	int straightOrFlushScore = getStraightOrFlushScore();
	int pairScore = getPairScore();

	if (pairScore>straightOrFlushScore)
	{
		handScore = pairScore;
	}
	else
	{
		handScore = straightOrFlushScore;
	}

	/*	p1 = TH 6H 9H QH JH [(1, 1, 1, 1, 1), (12, 11, 10, 9, 6)], score = 0
		p2 = 9H 4D JC KS JS [(2, 1, 1, 1), (11, 13, 9, 4)], score = 1

		p1 = 7C 7S KC KS JC [(2, 2, 1), (13, 7, 11)], score = 2
		p2 = 7H 7D KH KD 9S [(2, 2, 1), (13, 7, 9)], score = 2*/
}

int PokerHand::getPairScore()
{
	int i = 0;
	for each(vector<int> ref in handPairFreqRank)
	{
		if (ref == cardFreqVector)
		{
			return i + 1;//because first index is 0 but the rank is 1
		}
		i++;
	}

	throw exception("invalid freq vector");
}

int PokerHand::getStraightOrFlushScore()
{
	if (isRoyalFlush())
	{
		return 10;
	}
	if (isStraightFlush())
	{
		return 9;
	}
	if (isFlush())
	{
		return 6;
	}
	if (isStraight())
	{
		return 5;
	}

	return 0;
}

bool PokerHand::isRoyalFlush()
{
	bool valueCheck = 0;
	bool sameSuitCheck = 0;
	vector<int> royalFlush = { 14,13,12,11,10 };

	if (cardValueVector == royalFlush)
	{
		valueCheck = 1;
	}
	if (cardFreqVector.size() == 1)
	{
		sameSuitCheck = 1;
	}


	if (sameSuitCheck && valueCheck)
	{
		return 1;
	}
	else
	{
		return 0;
	}

}

bool PokerHand::isStraightFlush()
{
	bool consecutiveCheck = 1;
	for (int i = 0; i < cardValueVector.size() - 1; ++i)
	{
		if (cardValueVector[i] != cardValueVector[i + 1] + 1)
		{
			consecutiveCheck = 0;
			break;
		}
	}


	if (consecutiveCheck && allSuitsTheSame)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

bool PokerHand::isFlush()
{
	if (allSuitsTheSame)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

bool PokerHand::isStraight()
{
	if(cardValueVector.size()<5)
	{
		return 0;
	}
	for (int i = 0; i < cardValueVector.size() - 1; ++i)
	{
		if (cardValueVector[i] != cardValueVector[i + 1] + 1)
		{
			return 0;
		}
	}

	return 1;
}

void PokerHand::sortPokerHandVectors()
{
	int temp;
	//bubble sort values and its frequencies the same way
	for (int i = 0; i < cardValueVector.size() - 1; ++i)
	{
		for (int j = 0; j < cardValueVector.size() - 1 - i; ++j)
		{
			if (cardValueVector[j]<cardValueVector[j + 1])
			{
				temp = cardValueVector[j + 1];
				cardValueVector[j + 1] = cardValueVector[j];
				cardValueVector[j] = temp;

				temp = cardFreqVector[j + 1];
				cardFreqVector[j + 1] = cardFreqVector[j];
				cardFreqVector[j] = temp;
			}
		}
	}
	//bubble sort frequencies and its values the same way 
	for (int i = 0; i < cardFreqVector.size() - 1; ++i)
	{
		for (int j = 0; j < cardFreqVector.size() - 1 - i; ++j)
		{
			if (cardFreqVector[j]<cardFreqVector[j + 1])
			{


				temp = cardFreqVector[j + 1];
				cardFreqVector[j + 1] = cardFreqVector[j];
				cardFreqVector[j] = temp;

				temp = cardValueVector[j + 1];
				cardValueVector[j + 1] = cardValueVector[j];
				cardValueVector[j] = temp;
			}
		}
	}

	//finished result is a descending freqVector and a descending valueVector(depends on the freqVector and the freq subvectors which contain the same values)
	/*	p1 = TH 6H 9H QH JH [(1, 1, 1, 1, 1), (12, 11, 10, 9, 6)]
	p2 = 9H 4D JC KS JS [(2, 1, 1, 1), (11, 13, 9, 4)]

	p1 = 7C 7S KC KS JC [(2, 2, 1), (13, 7, 11)]
	p2 = 7H 7D KH KD 9S [(2, 2, 1), (13, 7, 9)]*/
}

int PokerHand::getValuePositionInVector(int ref)
{
	for (int i = 0; i<cardValueVector.size(); i++)
	{
		if (ref == cardValueVector[i])
		{
			return i;
		}
	}

	throw exception("error");
}


bool PokerHand::isValueInVector(int ref)
{
	for each(int a in cardValueVector)
	{
		if (a == ref)
		{
			return 1;
		}
	}
	return 0;
}

int PokerHand::getCardValueFromChar(char ref)
{
	switch (ref)
	{

	case '2':
		return 2;
	case '3':
		return 3;

	case '4':
		return 4;

	case '5':
		return 5;

	case '6':
		return 6;

	case '7':
		return 7;

	case '8':
		return 8;

	case '9':
		return 9;

	case 'T':
		return 10;

	case 'J':
		return 11;

	case 'Q':
		return 12;

	case 'K':
		return 13;

	case 'A':
		return 14;
	default:
		return -1;
	}


}

void PokerHand::addValueToVectors(char cardValue)
{
	char cardValueChar;
	int cardValueInt;

	cardValueChar = cardValue;
	cardValueInt = getCardValueFromChar(cardValueChar);
	if (!isValueInVector(cardValueInt))
	{
		cardValueVector.emplace_back(cardValueInt);
		cardFreqVector.emplace_back(1);
	}
	else
	{
		int valueIndex = getValuePositionInVector(cardValueInt);
		cardFreqVector[valueIndex]++;
	}
}

void PokerHand::updateAllSuitsSame(char cardSuit, char firstCardSuit)
{
	if (firstCardSuit != cardSuit)
	{
		allSuitsTheSame = 0;
	}
}

int PokerHand::getHandScore()
{
	return handScore;
}


void splitStringInTwo(string bothPlayerHands,string &player1String,string &player2String)
{
	player1String = bothPlayerHands.substr(0, bothPlayerHands.length() / 2);
	player2String = bothPlayerHands.substr(bothPlayerHands.length() / 2 + 1);
	player1String.erase(remove_if(player1String.begin(), player1String.end(), isspace), player1String.end());
	player2String.erase(remove_if(player2String.begin(), player2String.end(), isspace), player2String.end());
}





int main()
{
	ifstream is("poker.txt");
	if(!is.is_open())
	{
		return 1;
	}

	string bothPlayerHands;
	string player1String;
	string player2String;

	PokerHand* player1Hand=nullptr;
	PokerHand* player2Hand=nullptr;

	int player1WinCounter=0;

	time_t t1 = clock();
	while(true)
	{
		if (is.eof())break;
		getline(is, bothPlayerHands);
		
		splitStringInTwo(bothPlayerHands, player1String, player2String);
		
		player1Hand = new PokerHand(player1String);
		player2Hand = new PokerHand(player2String);

		if(*player1Hand > *player2Hand)
		{
			player1WinCounter++;
		}
		

		delete(player1Hand);
		delete(player2Hand);
		
	}
	time_t t2 = clock();
	cout << player1WinCounter<< "  "<<t2-t1;

	
    return 0;
}

