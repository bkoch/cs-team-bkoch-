// CS academy flip game.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <math.h>
using namespace std;

bool** createMatrix(bool** matrix,int rowCount,int colCount)
{
	matrix = new bool*[rowCount];
	for (int i = 0; i < rowCount; ++i)
		matrix[i] = new bool[colCount];

	return matrix;
}

void fillMatrix(bool** matrix,int rowCount,int colCount)
{
	for (int i = 0; i < rowCount; ++i)
	{
		for (int j = 0; j < colCount; ++j)
		{
			cin >> matrix[i][j];
		}
	}
}

long long int getBinaryPositionDecimalValue(int colCount, int j)
{
	return pow(2, colCount-1 - j);
}

void printMatrix(bool** matrix,int rowCount,int colCount)
{
	for (int i = 0; i < rowCount; ++i)
	{
		for (int j = 0; j < colCount; ++j)
		{
			cout<< matrix[i][j]<<" ";
		}
		cout << endl;
	}
	cout << endl;
}

void printBinaryValuesArray(int* ref,int rowCount)
{
	for (int i = 0; i < rowCount; ++i)
	{
		cout << ref[i] << " ";
	}
	cout << endl;
}

void invertColumnValues(bool** matrix,int rowCount,int targetedColumn)
{
	for(int i=0;i<rowCount;i++)
	{
		matrix[i][targetedColumn] = !matrix[i][targetedColumn];
	}
}

void invertRowValues(bool** &matrix,int colCount,int targetedRow)
{
	for(int i=0;i<colCount;i++)
	{
		matrix[targetedRow][i] = !matrix[targetedRow][i];
	}
}

void invertColumns(bool** &matrix, int rowCount, int colCount)
{
	int colFalseCounter = 0;

	for (int i = 0; i < colCount; ++i)
	{
		for (int j = 0; j < rowCount; ++j)
		{
			if (matrix[j][i] == 0)
			{
				colFalseCounter++;
			}
		}
		if (colFalseCounter > rowCount / 2)
		{
			invertColumnValues(matrix, rowCount, i);
		}
		colFalseCounter = 0;
	}
	//printMatrix(matrix, rowCount, colCount);
}

void invertRows(bool** &matrix, int rowCount, int colCount)
{
	for (int i = 0; i < rowCount; ++i)
	{
		if (matrix[i][0] == 0)
		{
			invertRowValues(matrix, colCount, i);
		}
	}
	//printMatrix(matrix, rowCount, colCount);
}

void maxModifyTheMatrix(bool** &matrix,int rowCount,int colCount)
{
	invertRows(matrix, rowCount, colCount);

	invertColumns(matrix, rowCount, colCount);

	printMatrix(matrix, rowCount, colCount);
}

long long int getMatrixDecimalValue(bool** matrix, int rowCount, int colCount)
{
	long long int sum = 0;

	for (int i = 0; i < rowCount; ++i)
	{
		for (int j = 0; j < colCount; ++j)
		{
			if(matrix[i][j]==1)
			{
				sum += getBinaryPositionDecimalValue(colCount, j);
			}
		}
	}

	return sum;
}



int main()
{

	int rowCount, colCount;
	cin >> rowCount >> colCount;

	bool** matrix=nullptr;
	
	matrix = createMatrix(matrix, rowCount, colCount);

	fillMatrix(matrix, rowCount, colCount);

	//cout << endl;

	maxModifyTheMatrix(matrix, rowCount, colCount);

	//printMatrix(matrix, rowCount, colCount);

	cout << getMatrixDecimalValue(matrix, rowCount, colCount);





    return 0;
}

