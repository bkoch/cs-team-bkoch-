// CP3 Spiral Tap.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
using std::cout;
using std::cin;
using std::endl;

enum Direction{up,left,down,right};

struct point
{
	int x;
	int y;
};

int main()
{
	int matrixSize;
	int spiralPoint;

	point squareMiddlePoint;
	while(true)
	{
		cin >> matrixSize;
		cin >> spiralPoint;
		if (matrixSize==0)
		{
			break;
		}

		squareMiddlePoint.x = matrixSize / 2 + 1;
		squareMiddlePoint.y = matrixSize / 2 + 1;
	

		int directionDuration=1;
		int directionTurn = up;
		int durationChangeFlag = 0;
	

		int stepCounter = 1;
		while(stepCounter<spiralPoint)
		{

			directionTurn = directionTurn % 4;
			durationChangeFlag++;

			for(int j=0;j<directionDuration && stepCounter<spiralPoint;j++)
			{
				if (directionTurn == up)
				{
					squareMiddlePoint.x += 1;
				}
				else if (directionTurn == left)
				{
					squareMiddlePoint.y -= 1;
				}
				else if (directionTurn == down)
				{
					squareMiddlePoint.x -= 1;
				}
				else if (directionTurn == right)
				{
					squareMiddlePoint.y += 1;
				}

				stepCounter++;

			}
			if(durationChangeFlag%2==0)
			{
				directionDuration++;
			}
			directionTurn++;
		}
		cout << "Line = " << squareMiddlePoint.x << " " << "Column = " << squareMiddlePoint.y << endl;


	}
    return 0;
}

