// CP3 army buddies.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;
/*
 * algoritam radi tako da prvo nade gdje su ti vojnici koji se brisu kad ih je nasao provjerava 
 * da li su na granicama ili ne itd.
 * Doslovno je onda samo igranje s pravilnim brisanjem i pravilnim ispisom pomocu iteratorima itd
 * 
 */




int main()
{


	int noOfSoldiers;
	int noOfLossReports;

	int lossReportLeft;
	int lossReportRight;
	

	

	vector<int> soldiers;
	vector<int>::iterator itr1;
	vector<int>::iterator itr2;
	

	while(true)
	{

		cin >> noOfSoldiers;
		cin >> noOfLossReports;
		if(noOfSoldiers==0 && noOfLossReports==0)
		{
			break;
		}


		for(int i=0;i<noOfSoldiers;i++)
		{
			soldiers.push_back(i + 1);
		}


		for(int i=0;i<noOfLossReports;i++)
		{
			cin >> lossReportLeft;
			cin >> lossReportRight;
			itr1=lower_bound(soldiers.begin(), soldiers.end(), lossReportLeft); 
			itr2 = lower_bound(soldiers.begin(), soldiers.end(), lossReportRight);

			if(lossReportLeft==soldiers.front())
			{
				cout << "*" << " ";

				if (lossReportRight == soldiers.back())//both end values
				{
					cout << "*" << endl;
					soldiers.clear();
				}
				else//first end value
				{
					cout << *(itr2 + 1) << endl;
					soldiers.erase(itr1, itr2 + 1);
				}
			}
			else
			{
				cout << *(itr1 - 1) << " ";

				if (lossReportRight == soldiers.back())//second end value
				{
					cout << "*" << endl;
					soldiers.erase(itr1,soldiers.end());
				}
				else//no end values
				{
					cout << *(itr2 + 1) << endl;
					soldiers.erase(itr1, itr2 + 1);
				}
			}		
		}

		soldiers.clear();
		cout << "-" << endl;
	}
    return 0;
}

