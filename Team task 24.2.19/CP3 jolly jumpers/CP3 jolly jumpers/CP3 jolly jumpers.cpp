// CP3 jolly jumpers.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
using namespace std;

int main()
{
	vector<int> intVector;
	int noOfElements;
	cin >> noOfElements;
	for(int i=0;i<noOfElements;++i)
	{
		int temp;
		cin >> temp;
		intVector.push_back(temp);
	}

	vector<int> diffValues;
	for(int i=0;i<noOfElements-1;++i)
	{
		diffValues.push_back(abs(intVector[i] - intVector[i + 1]));
	}
	sort(diffValues.begin(), diffValues.end(),greater<int>());

	bool isJollyJumper = true;
	for (int i = 0; i<diffValues.size() - 1; ++i)
	{
		if((diffValues[i] - diffValues[i + 1]) != 1)
		{
			isJollyJumper = false;
			break;
		}
	}

	if(isJollyJumper)
	{
		cout << "Jolly" << endl;
	}
	else
	{
		cout << "Not jolly" << endl;
	}


    return 0;
}

