// ProjectEuler-Coded Triangle Num Solution.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;
//A is 65
//Z is 90 in asci 
//treba oduzet 64 da bi se dobila alphabet value 


int getAlphabeticValue(char ref)
{
	return ref - 64;
}

int extractWordValue(ifstream& inputStream)
{
	int wordValue = 0;
	char symbol;
	while (true)
	{

		inputStream >> symbol;
		if (!inputStream.eof() && symbol != '\"' && symbol != ',')
		{
			wordValue += getAlphabeticValue(symbol);
		}
		else
		{
			return wordValue;
			break;
		}
	}

	return 0;
}

bool isTriangleNum(int ref)
{
	int triangleNum = 0;
	int n = 0;
	while (triangleNum<ref)
	{
		n++;
		triangleNum = n*(n + 1) / 2;
		if (triangleNum == ref)
		{
			return 1;
		}
	}

	return 0;
}

int getTriangleNumCount(vector<int>& wordValueVector)
{
	int triangleNumCounter = 0;
	for each(int ref in wordValueVector)
	{
		if(isTriangleNum(ref))
		{
			triangleNumCounter++;
		}
	}

	return triangleNumCounter;
}

int main()
{	
	ifstream inputStream;
	inputStream.open("words.txt");

	vector<int> wordValueVector;

	while (true)
	{
		
		wordValueVector.emplace_back(extractWordValue(inputStream));
		if(inputStream.eof())
		{
			break;
		}
	}


	cout << getTriangleNumCount(wordValueVector);

    return 0;
}

