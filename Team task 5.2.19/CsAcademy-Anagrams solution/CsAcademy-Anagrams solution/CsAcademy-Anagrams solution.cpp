// CsAcademy-Anagrams solution.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <iostream>
#include <vector>
using namespace std;

struct stringFrequency
{
	string var;
	int freq;
};

bool isCharEquivalent(stringFrequency a,stringFrequency b)
{
	if(a.var.size()!= b.var.size())
	{
		return 0;
	}

	bool quitFlag = 1;
	for(int i=0;i<a.var.length();i++)
	{
		for(int j=0;j<b.var.length();j++)
		{
			if(a.var[i]==b.var[j])
			{
				a.var[i] = 0;
				b.var[j] = 0;
				quitFlag = 0;
			}
			
		}
		if (quitFlag == 1)
		{
			return 0;
		}
		quitFlag = 1;
	}

	return 1;
}

int extractMaxFreq(vector<stringFrequency>& stringFreqVector,unsigned int wordCount)
{
	int maxFreq = 0;
	for (int i = 0; i<wordCount; i++)
	{
		if (stringFreqVector[i].freq>maxFreq)
		{
			maxFreq = stringFreqVector[i].freq;
		}
	}

	return maxFreq;
}

void findCharEquivalentWords(int wordCount,vector<stringFrequency>& stringFreqVector)
{
	for (int i = 0; i<wordCount; i++)
	{
		for (int j = i + 1; j<wordCount; j++)
		{
			if (isCharEquivalent(stringFreqVector[i], stringFreqVector[j]) && stringFreqVector[i].freq != -1 && stringFreqVector[j].freq != -1)
			{
				stringFreqVector[i].freq++;
				stringFreqVector[j].freq = -1;
			}
		}
	}
}



int main()
{
	int wordCount;
	vector<stringFrequency> stringFreqVector;

	cin >> wordCount;

	stringFrequency temp;
	for (int i = 0; i < wordCount; ++i)
	{
		cin >> temp.var;
		temp.freq = 1;

		stringFreqVector.emplace_back(temp);
	}

	findCharEquivalentWords(wordCount, stringFreqVector);
	
	

	cout << extractMaxFreq(stringFreqVector, wordCount);

	

    return 0;
}

